const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (error, result) {
        res.status(200).json({
            bicicletas: result
        });
    });
}

exports.bicicleta_create = function (req, res) {
    var bicicleta = new Bicicleta({
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    });

    Bicicleta.add(bicicleta, function (error, newElement) {
        res.status(200).json({
            bicicleta: newElement
        });
    })
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.code, function (err) {
        res.status(204).send();
    });
}