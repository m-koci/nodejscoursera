var mymap = L.map('main_map').setView([-34.888488, -56.146273], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'sk.eyJ1IjoibWtvY2kiLCJhIjoiY2tlcDVobXR3MGlmeTJwbmlrZDF0cmIweCJ9.VOcS-Hvxiloti_p0bKlDdg'
}).addTo(mymap);

//var marker = L.marker([-34.888488, -56.146273]).addTo(mymap);


var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("Las coordenadas seleccionadas son: " + e.latlng.toString())
        .openOn(mymap);
}

mymap.on('click', onMapClick);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        result.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        })
    }
})