const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');

beforeEach(() => {
    console.log('Testeando...');
})

describe('Bicicleta API', () => {
    describe('GET Bicicletas', () => {
        it('should response status 200', function () {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.888488, -56.146273]);
            Bicicleta.add(a);
            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200);
            })
        });
    })

    describe('POST Bicicletas /create', () => {
        it('should response status 200', function (done) {
            var headers = {'content-type': 'application/json'};
            var a = '{"id":3, "color":"rojo","modelo": "urbana","lat":-34.888488, "lng":-56.146273}';
            request.post({
                headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: a
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(3).color).toBe("rojo");
                done();
            })
        });
    })

})